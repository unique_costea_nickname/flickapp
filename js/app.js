var flickrApp = angular.module('flickrApp', ['ngRoute', 'ngSanitize']);

flickrApp.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
	$locationProvider.html5Mode({
		enabled: true,
		requireBase: false
	});
	$routeProvider
		.when('/flickr/', {
			controller: 'MainController',
			templateUrl: '/flickr/views/archive.html',
			reloadOnSearch: false,
		})
		.when('/flickr/image/:index', {
			controller: 'SingleController',
			templateUrl: '/flickr/views/single.html'
		})
		.otherwise({redirectTo: '/flickr/'});


}]);
