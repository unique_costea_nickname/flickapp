flickrApp.controller('SingleController', ['$scope', '$sce', '$rootScope', '$routeParams', 'loadDataService', function($scope, $sce, $rootScope, $routeParams, loadDataService) {

	$rootScope.$emit('getImage', $routeParams.index );
	$rootScope.$on('imageLoaded', function (event, img) {
		console.log(img);
		$scope.img = img;
		$scope.tagList = img.tags.split(' ');
		$scope.desc = $sce.trustAsHtml( img.description );
	});
}]);