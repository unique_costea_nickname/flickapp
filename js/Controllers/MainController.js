flickrApp.controller('MainController', ['$scope', '$rootScope', '$location', '$routeParams', 'loadDataService', function ($scope, $rootScope, $location, $routeParams, loadDataService) {

	$rootScope.$on('getImage', function (event, index) {
		$rootScope.$emit('imageLoaded', $scope.photos[index] );
	})

	$scope.search = function () {
		$location.search('keyword', $scope.searchValue);

		var that = loadDataService.getData( $scope.searchValue ).then(function (result) {
			$scope.photos = result.items;
		});
	}

	if ($routeParams.keyword != undefined) {
		$scope.searchValue = $routeParams.keyword;
	}else {
		$scope.searchValue = 'potato';
	}
	$scope.search();
}]);