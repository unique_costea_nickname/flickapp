flickrApp.service('loadDataService', function ($http) {

	this.getData = function (keyword) {

		var url = 'http://api.flickr.com/services/feeds/photos_public.gne?tags='+ keyword +'&tagmode=all&format=json&jsoncallback=JSON_CALLBACK';

		return $http({
			method: 'JSONP',
			url: url,
			cache: true,
		}).then(function (result) {
			return result.data;
		})
	}

});