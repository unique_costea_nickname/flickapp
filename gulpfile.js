/*******************************************************************************
1. DEPENDENCIES
*******************************************************************************/

var gulp = require('gulp');                             // gulp core
    sass = require('gulp-sass'),                        // sass compiler
    uglify = require('gulp-uglify'),                    // uglifies the js
    jshint = require('gulp-jshint'),                    // check if js is ok
    rename = require("gulp-rename");                    // rename files
    concat = require('gulp-concat'),                    // concatinate js
    notify = require('gulp-notify'),                    // send notifications to osx
    plumber = require('gulp-plumber'),                  // disable interuption
    stylish = require('jshint-stylish'),                // make errors look good in shell
    minifycss = require('gulp-minify-css'),             // minify the css files
    autoprefixer = require('gulp-autoprefixer'),        // sets missing browserprefixes
    bourbon = require('node-bourbon'),
    sprite = require('gulp-sprite-generator');

/*******************************************************************************
2. FILE DESTINATIONS (RELATIVE TO ASSSETS FOLDER)
*******************************************************************************/

var	target = {
    sass_gen : 'sass/general.scss',
	sass_fonts_gen : 'sass/fonts.scss',
	sass_src : 'sass/*.scss',
	css_dest : 'css',
	js_src   : 'js/*.js',
	js_dest  : 'js/min',
    img_src  :   "img/",
};

/*******************************************************************************
3. SASS TASK
*******************************************************************************/

gulp.task('sass', function() {
  return gulp.src(target.sass_gen)
    .pipe(plumber())
    .pipe(sass({ includePaths: require('node-bourbon').includePaths, sourceComments: 'normal', errLogToConsole: true }))
    .pipe(autoprefixer(
            'last 2 version',
            '> 1%',
            'ie 8',
            'ie 9',
            'ios 6',
            'android 4'
        ))
    .pipe(minifycss())
    .pipe(gulp.dest(target.css_dest))
    .pipe(notify({message: 'SCSS processed!'}));
});

gulp.task('fonts_sass', function() {
  return gulp.src(target.sass_fonts_gen)
  	.pipe(plumber())
    .pipe(sass({ sourceComments: 'map', errLogToConsole: true }))
    .pipe(minifycss())
    .pipe(gulp.dest(target.css_dest))
    .pipe(notify({message: 'SCSS processed!'}));
});

/*******************************************************************************
4. JS TASKS
*******************************************************************************/

gulp.task('js', function(){
	return gulp.src(target.js_src)
		.pipe(jshint())
     	.pipe(jshint.reporter(stylish))
        .pipe(concat('production.js'))
		.pipe(uglify({
            mangle: false,
        }))
		.pipe(gulp.dest(target.js_dest))
		.pipe(notify({ message: 'JS processed!'}));
});



/*******************************************************************************
5. GULP WATCH
*******************************************************************************/

gulp.task('watch', function(){
    gulp.watch(target.sass_src, function(){
        gulp.run('sass');
        gulp.run('fonts_sass');
    });

	gulp.watch(target.js_src, function(){
		gulp.run('js');
	});

});

/*******************************************************************************
6. GULP TASKS
*******************************************************************************/

gulp.task('default', function(){
	gulp.run(
        'sass',
        'fonts_sass',
        'js',
        'watch'
    );
});